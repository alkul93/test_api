<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
            'name' => 'task1',
            'description' => 'task description1',
            'created_at' => (new \DateTime)->format('Y-m-d H:i:s'),
            'updated_at' => (new \DateTime)->format('Y-m-d H:i:s'),
        ]);

        DB::table('tasks')->insert([
            'name' => 'task2',
            'description' => 'task description2',
            'created_at' => (new \DateTime)->format('Y-m-d H:i:s'),
            'updated_at' => (new \DateTime)->format('Y-m-d H:i:s'),
        ]);

        DB::table('tasks')->insert([
            'name' => 'task3',
            'description' => 'task description3',
            'created_at' => (new \DateTime)->format('Y-m-d H:i:s'),
            'updated_at' => (new \DateTime)->format('Y-m-d H:i:s'),
        ]);
    }
}
