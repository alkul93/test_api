<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\TagResource;
use App\Tag;

class Task extends Model
{
    use \App\Traits\WithAdditionalTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    /**
     * The tags that belong to the tasks.
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    /**
     * @inheritdoc
     */
    public function toArray()
    {
        $result = [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'created_at' => (new \DateTime())->format('d.m.Y H:i'),
        ];

        if (self::$withAdditionalData) {
            return $result + ['tags' => TagResource::collection($this->tags)];
        }

        return $result;
    }
}
