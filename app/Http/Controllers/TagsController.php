<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\TagResource;
use App\Tag;

class TagsController extends Controller
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
        ]);
    }

    /**
     * Metohod return all tags
     *
     * @return array
     */
    public function index()
    {
        return TagResource::collection(Task::all());
    }

    /**
     * Method return one tag with tasks
     *
     * @param integer $id Tag ID
     * @return array
     */
    public function show($id)
    {
        return new TagResource(Tag::withAdditional()->find($id));
    }

    /**
     * Create a new tag instance after validation.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Tag
     */
    protected function store(Request $request)
    {
        return Tag::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
        ]);
    }
}
