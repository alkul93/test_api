<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\TaskResource;
use App\Task;

class TasksController extends Controller
{
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'description' => ['string',],
        ]);
    }

    /**
     * Metohod return all tasks
     *
     * @return array
     */
    public function index()
    {
        return TaskResource::collection(Task::all());
    }

    /**
     * Method return one task with tags
     *
     * @param integer $id Task ID
     * @return array
     */
    public function show($id)
    {
        return new TaskResource(Task::withAdditional()->find($id));
    }

    /**
     * Create a new task instance after validation.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Task
     */
    protected function store(Request $request)
    {
        return Task::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
        ]);
    }
}
