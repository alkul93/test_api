<?php

namespace App\Traits;


/**
 *
 */
trait WithAdditionalTrait
{
    /**
     * @var boolean Flag, add additional data or not
     */
    public static $withAdditionalData = false;

    /**
     * Make query with additional data.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public static function withAdditional()
    {
        static::$withAdditionalData = true;
        return new static;
    }
}
