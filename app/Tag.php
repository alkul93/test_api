<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Resources\TaskResource;
use App\Task;

class Tag extends Model
{
    use \App\Traits\WithAdditionalTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * The tasks that belong to the tag.
     */
    public function tasks()
    {
        return $this->belongsToMany('App\Task');
    }

    /**
     * @inheritdoc
     */
    public function toArray()
    {
        $result = [
            'id' => $this->id,
            'name' => $this->name,
            'created_at' => (new \DateTime())->format('d.m.Y H:i'),
        ];

        if (self::$withAdditionalData) {
            return $result + ['tasks' => TaskResource::collection($this->tasks)];
        }

        return $result;
    }
}
